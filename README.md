# SKID

**SKID module is used to share variables from the SKID PLCs with the CA.**


## Files

* **db/skid.db** : holds the db generated to communicate with the PLC program
* **startup/skid.cmd** : starts the IOC

## Records

**see skid.db**


## Database macros

**no MACRO**

## Module OPIs

* **opi/EPICS-Main_View.opi**: master view to control the SKID, with installation summary and access to the other views
* **opi/EPICS-Regulations**: view to set control loops parameters and check the measurments in graphs
* **opi/EPICS-Errors**: summary view with all the error on the SKID installation

## How to use the application

This module is not meant to be used in another IOC.

0/ Install if not already available:
     make install
     # or make install LIBVERSION=foo
     # otherwise the default version will be the username

1/ Run the ioc with the following command :
    LIBVERSION=fgohier
    SKID_PLC_IP=10.0.0.10
    iocsh -r skid-rfq,${LIBVERSION} -c requireSnippet\(skid.cmd,"IPADDR=${SKID_PLC_IP}"\)

2/ Import the OPIs in CSS, and execute the EPICS-Main_View.OPI
