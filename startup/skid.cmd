# @field IPADDR
# @type STRING
# PLC IP address

require skid-rfq, 1.3+
require autosave, 5.7.0

#s7plcConfigure (PLCname, IPaddr, port, inSize, outSize, bigEndian, recvTimeout, sendIntervall)
#s7plcConfigure("ESS_Skid", $(IPADDR), "2000", 292, 122, 1, 5000, 1000)
s7plcConfigure("ESS_SKID", $(IPADDR), 2000, 292, 122, 1, 5000, 1000)
var s7plcDebug 0

#**********************************************************#
#************************ AUTOSAVE ************************#
#**********************************************************#

#-----------------------------------------------------------
# CONFIGURE AUTOSAVE
#-----------------------------------------------------------
#save_restoreSet_Debug(0)
# Number of sequenced backup files to write
#save_restoreSet_NumSeqFiles(1)

# Specify directories in which to search for request files
set_requestfile_path("../misc/","")
set_requestfile_path("./misc/","")

# Specify where save files should be
set_savefile_path("/tmp/")

# Specify what save files should be restored
set_pass0_restoreFile("iocSkid.sav")


#-----------------------------------------------------------
# INIT IOC
#-----------------------------------------------------------

dbLoadRecords("skid.db") 

iocInit

#-----------------------------------------------------------
# START AUTOSAVE
#-----------------------------------------------------------

create_monitor_set("iocSkid.req",1,"")

