TODO MANUALLY
=============

## OPIs

Things that still need doing manually after generating the OPIs from DXF files.

### Main View
+ Change DPhase (in Vane regulation loop) **Widget Type** from *Text Input* to *Text Update*, **Background Color** to *black (0,0,0)* and **Foreground Color** to *yellow (240,240,0)*

### Regulations
+ For all 18 PID settings set **Maximum** to *200* and **Minimum** to *0*
+ For 5 setpoints (all but DPhase regulation) **Text Input box** set **Maximum** to *50* (°C) and **Minimum** to *0* (°C)
+ Change DPhase measure (in Vane regulation loop) **Widget Type** from *Text Input* to *Text Update*

### Errors
*NA*

## Database

Things to check in generated database.

NOTE: when generating the new database using *PlcParserTool*, choose *S7PLC* protocol and **uncheck the "_Generate Input Records ?_" option**.

### Acknowledgment `HIGH`
Acknowledgment BO records should hold high only for 1 seconds
Currently there are 25 acknowledgment:

    CWM-CWS03:WtrC-PCV-011:Ack
    CWM-CWS03:WtrC-FCV-011:Ack
    CWM-CWS03:WtrC-FCV-012:Ack
    CWM-CWS03:WtrC-FCV-013:Ack
    CWM-CWS03:WtrC-FCV-014:Ack
    CWM-CWS03:WtrC-FCV-015:Ack
    CWM-CWS03:WtrC-FCV-020:Ack
    CWM-CWS03:WtrC-FCV-021:Ack
    CWM-CWS03:WtrC-FCV-030:Ack
    CWM-CWS03:WtrC-FCV-040:Ack
    CWM-CWS03:WtrC-FCV-050:Ack
    CWM-CWS03:WtrC-FCV-051:Ack
    CWM-CWS03:WtrC-P-10:Ack
    CWM-CWS03:WtrC-P-20:Ack
    CWM-CWS03:WtrC-P-30:Ack
    CWM-CWS03:WtrC-P-40:Ack
    CWM-CWS03:WtrC-P-50:Ack
    CWM-CWS03:WtrC-PT-017:ACK
    CWM-CWS03:WtrC-FT-011:ACK
    CWM-CWS03:WtrC-LT-070:LHACK
    CWM-CWS03:WtrC-LT-070:LLACK
    CWM-CWS03:WtrC-LT-070:LVLACK
    CWM-CWS03:WtrC-EC-010:Ack
    CWM-CWS03:WtrC-CT-020:Ack
    CWM-CWS03:WtrC-FS:Ack
